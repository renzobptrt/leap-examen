
/*
Asumo: Que los puntos que se indiquen por parametro a la funcion que yo cree, se dibujaran de tal forma
que formen una circunferencia, ante esto resuelvo. 
Lenguaje : C#
*/

// (x,y) coordenada del centro de la semicircunferencia y r es el radio
// numPunt : Numero de puntos para dibujar la semicircunferencia
void PuntosSemiCircunf(int x,int y,int numPunt, float r){ 
    //Minimo 3 puntos para dibujar dentro de una "semicircunferencia"
    if(numPunt < 3){
        Debug.Log("No es numero de puntos valido);
        return; //Sale de la funcion
    }
    else{
        int numDib = numPunt - 1;
        float a = 0f, b = 0f;
        for(i = 0; i< Mathf.pi/2 ; i + (Mathf.pi / numDib)){
            a = Mathf.cos(i) * r;
            b = Mathf.sin(i) * r;
            DibujarPunto( x + a, y + b ); //Funcion usada para dibujar un punto en la escena
            DibujarPunto( x - a, y + b); //Funcion usada para dibujar un punto en la escena
        }
        if(numPunt%2!=0)
            DibujarPunto(x, y + r)
    }
}