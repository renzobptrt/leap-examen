﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class isConnected : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D col){
        //Si detecta conexion
        if(col.gameObject.tag.Equals("CheckConnect")){
            GameManager.sharedInstance.AddScore();
        }
    }

    void OnTriggerExit2D(Collider2D col){
        //Si salen de la conexion
        if(col.gameObject.tag.Equals("CheckConnect")){
            GameManager.sharedInstance.RestScore();
        }
    }
}
