﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameManager : MonoBehaviour
{
    public GameObject[] tubes;

    public static GameManager sharedInstance;
    public GameObject winPanel;

    private int score = 0;
    // Start is called before the first frame update
    void Start()
    {
        if(sharedInstance!=null){
            Destroy(this.gameObject);
        }
        else{
            sharedInstance = this;
        }
    }

    public void AddScore(){
        this.score++;
        Debug.Log(this.score);
        if(score == 20){
            Debug.Log("Ganaste");
            winPanel.gameObject.SetActive(true);
        }
    }

    public void RestScore(){
        this.score--;
        Debug.Log(this.score);
    } 
}
