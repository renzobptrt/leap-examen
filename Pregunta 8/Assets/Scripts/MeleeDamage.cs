﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeDamage : MonoBehaviour
{   


    private void OnTriggerEnter2D(Collider2D collider){
        //Si detecta que el ataque ejercio sobre el enemigo
        if(collider.gameObject.tag.Equals("Enemy")){
            //Llamar a la función de daño
            collider.GetComponent<Enemy>().OnMeleeAttackConnected();
        }
    }
}
