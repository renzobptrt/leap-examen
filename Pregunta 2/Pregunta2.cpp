#include <iostream>
#include <math.h>
using namespace std;


//datos del rectangulo
struct rectangulo{
	//coordenada central
	float x;
	float y;
	//otros datos
	float ancho;
	float alto;
};

bool colision(rectangulo A, rectangulo B); //logica de colision entre dos rectangulos
void param(rectangulo &A); //ingreso de datos del rectangulo

int main(){
	//declaracion de variables
	rectangulo A, B, C;	
	
	cout<<"Rectangulo 1:\n";
	param(A); 
	cout<<"Rectangulo 2:\n";
	param(B);
	cout<<"Rectangulo 3:\n";
	param(C);
	
	if(colision(A, B)) cout<<"A y B colisionan\n";
	if(colision(A, C)) cout<<"A y C colisionan\n";
	if(colision(B, C)) cout<<"B y C colisionan\n";
	
	if(colision(A, B) && colision(B, C) && colision(A, C)){
		cout<<"Los tres rectangulos colisionan";
	}else if(colision(A, B) || colision(A, B) || colision(A, C)){
		cout<<"Solo dos rectangulos como maximo colisionan";
	}else{
		cout<<"No colisiona ninguna figura";
	}
}

void param(rectangulo &A){ //ingreso de datos del rectangulo
	
	cout<<"X: ";
	cin>>A.x;
	cout<<"Y: ";
	cin>>A.y;
	cout<<"Anchura: ";
	cin>>A.ancho;
	cout<<"Altura: ";
	cin>>A.alto;
	
}

bool colision(rectangulo A, rectangulo B){ //logica de colision entre dos rectangulos

	if(((abs(A.x-B.x) == (A.ancho+B.ancho)/2) || (abs(A.y-B.y) == (A.alto+B.alto)/2)) && 
		/*1ra Condicion:
			Con la diferencia de coordenadas X obtenemos el distanciamiento horizontal entre ambos rectangulos, 
			si esta es igual a la semisuma de de anchuras, refleja que dos lados se encuentran en un mismo eje, 
			analogamente pasa lo mismo con las coordenadas Y y la semisuma de alturas. 
			Sin embargo esto no asegura que ambas figuras colisionen, puesto que al tener dos lados en un mismo eje
			puede ocurrir que se encuentren en sectores distintos, pero inicialmente tenemos la distancia minima 
			entre centros que se debe cumplir para la colision.
		*/
		(pow(pow(A.x-B.x, 2) + pow(A.y-B.y, 2), 0.5) <= pow(pow((A.ancho+B.ancho)/2, 2) + pow((A.alto+B.alto)/2, 2), 0.5))){
		/*2da Condicion:
			En geometria, la distancia entre dos puntos se puede calcular de la siguiente manera:
			((X1-X2)^2 + (Y1-Y2)^2)^1/2
			Donde X e Y son coordenadas de los puntos, en este caso, los centros de los rectangulos.
			Pasa que la distancia maxima que puede haber en una colision, es la hipotenusa formada por las semisumas de 
			las alturas y anchuras de los rectangulos tomados como catetos respectivamente. Tomando esta distancia
			como referecia, y la distancia calculada entre 2 puntos, decimos que para que exista una colision,
			la distancia entre 2 puntos debe ser menor o igual a la diagonal antes dicha.5.6
		
		Entonces teniendo como referencia una distancia minima y maxima entre centros, podemos asegurar cuando dos rectangulos 
		estan colisionando
		*/
		return true;
	}else{
		return false;
	}
	
}


