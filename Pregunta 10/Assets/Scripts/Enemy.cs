﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    //Enumerador para diferenciar el tipo de enemigo
    public enum Enemies { Rana, Tronco, Mono }
    //Objeto del enumerador
    public Enemies en;

    //Variable para indicar el tipo de enemigo al que pertenece
    public string enemyType;
    //String de los tipos de ataques que tiene este enemigo
    public EnemyAttack[] attacks;
    //Variable para indicar el ataque actual que realizara el enemigo
    public EnemyAttack currentAttack;

    //Variable para indicar si el enemigo ha sido parte de un ataque en orquesta o no
    public bool isAttackedOrquestra;

    //Variables para que el enemigo tenga conocimiento si otro enemigo ataco o no
    public bool isOtherTerrestreAttack;
    public bool isOtherAereoAttack;

    // Start is called before the first frame update
    void Start()
    {
        //En un principio ningun enemigo realizo un ataque
        isOtherTerrestreAttack = false;
        isOtherAereoAttack = false;
        //Seleccionara el tipo de ataque inicial
        SelectAttack();
    }

    // Update is called once per frame
    void Update()
    {
        //Rana
        if (Input.GetKeyDown(KeyCode.Q) && en == Enemies.Rana)
        {
            //Si actualmente existe un ataque en orquesta...
            if (Orquestra.sharedInstance.isActiveOrquesta)
            {
                //El sistema de orquesta no permitira otro ataque indicado por teclado.
                Debug.Log("El sistema de orquesta no ha permitido el ataque de la rana");
            }
            else
            {
                //Si actualmente NO existe un ataque en orquesta...
                //Y no hay otro enemigo de tipo terrestre atacando
                if (!isOtherTerrestreAttack)
                {
                    //Realizara el ataque
                    Attack();
                    //Aumenta la cantidad de ataques actuales en una unidad
                    Orquestra.sharedInstance.currentNAttacks++;
                    //La cantidad de ataques actuales debe ser como maximo la cantidad indicada 
                    if (Orquestra.sharedInstance.currentNAttacks >= Orquestra.sharedInstance.N)
                    {
                        Orquestra.sharedInstance.currentNAttacks = Orquestra.sharedInstance.N;
                        //Desbloqueamos la disponibilidad de un proximo ataque en orquesta
                        Orquestra.sharedInstance.blockOrquestra = false;
                        Debug.Log("El número mínimo de ataques simples para desbloquear un ataque orquesta ha sido alcanzado.");
                    }
                    //Imprimimos por consola
                    Debug.Log("El sistema ha permitido el ataque de la rana");
                }
                //Si existe otro enemigo de tipo terrestre atacando...
                else
                {
                    //El sistema no permitira el ataque
                    Debug.Log("El sistema no ha permitido que la rana ataque porque un enemigo de su tipo esta atacando");
                }
            }

        }
        //Tronco
        if (Input.GetKeyDown(KeyCode.W) && en == Enemies.Tronco)
        {
            //Si actualmente existe un ataque en orquesta...
            if (Orquestra.sharedInstance.isActiveOrquesta)
            {
                //El sistema de orquesta no permitira otro ataque indicado por teclado.
                Debug.Log("El sistema de orquesta no ha permitido el ataque del tronco");
            }
            else
            {
                //Si actualmente NO existe un ataque en orquesta...
                //Y no hay otro enemigo de tipo terrestre atacando
                if (!isOtherTerrestreAttack)
                {
                    //Realizara el ataque
                    Attack();
                    //Aumenta la cantidad de ataques actuales en una unidad
                    Orquestra.sharedInstance.currentNAttacks++;
                    if (Orquestra.sharedInstance.currentNAttacks >= Orquestra.sharedInstance.N)
                    {
                        Orquestra.sharedInstance.currentNAttacks = Orquestra.sharedInstance.N;
                        //Desbloqueamos la disponibilidad de un proximo ataque en orquesta
                        Orquestra.sharedInstance.blockOrquestra = false;
                        Debug.Log("El número mínimo de ataques simples para desbloquear un ataque orquesta ha sido alcanzado.");
                    }
                    //Imprimimos por consola
                    Debug.Log("El sistema ha permitido el ataque del tronco");
                }
                //Si existe otro enemigo de tipo terrestre atacando...
                else
                {
                    //El sistema no permitira el ataque
                    Debug.Log("El sistema no ha permitido que el tronco ataque porque un enemigo de su tipo esta atacando");
                }
            }

        }
        //Mono
        if (Input.GetKeyDown(KeyCode.T) && en == Enemies.Mono)
        {
            //Si actualmente existe un ataque en orquesta...
            if (Orquestra.sharedInstance.isActiveOrquesta)
            {
                //El sistema de orquesta no permitira otro ataque indicado por teclado.
                Debug.Log("El sistema de orquesta no ha permitido el ataque del mono");
            }
            else
            {
                //Si actualmente NO existe un ataque en orquesta...
                //Y no hay otro enemigo de tipo aereo atacando
                if (!isOtherAereoAttack)
                {
                    //Realizara el ataque
                    Attack();
                    //Aumenta la cantidad de ataques actuales en una unidad
                    Orquestra.sharedInstance.currentNAttacks++;
                    if (Orquestra.sharedInstance.currentNAttacks >= Orquestra.sharedInstance.N)
                    {
                        Orquestra.sharedInstance.currentNAttacks = Orquestra.sharedInstance.N;
                        //Desbloqueamos la disponibilidad de un proximo ataque en orquesta
                        Orquestra.sharedInstance.blockOrquestra = false;
                        Debug.Log("El número mínimo de ataques simples para desbloquear un ataque orquesta ha sido alcanzado.");
                    }
                    //Imprimimos por consola
                    Debug.Log("El sistema ha permitido el ataque del mono");
                }
                //Si existe otro enemigo de tipo terrestre atacando...
                else
                {
                    //El sistema no permitira el ataque
                    Debug.Log("El sistema no ha permitido que el mono ataque porque un enemigo de su tipo esta atacando");
                }
            }
        }
    }

    //Función de ataque
    public void Attack()
    {
        //Primero selecciona el tipo de ataque a realizar
        SelectAttack();
        //Indica a la clase orquesta que reciba la señal de que este enemigo ha realizado el ataque
        Orquestra.sharedInstance.RecibirSeñal(this);
        //Empezamos la corrutina para controlar el tiempo que demora este ataque en realizarse
        StartCoroutine(CountCurrentTimeToNextAttack());
    }

    //Funcion que devuelve los ataques disponibles del enemigo
    EnemyAttack[] GetEnemyAttacks()
    {
        return attacks;
    }

    //Seleccionamos el ataque
    void SelectAttack()
    {
        //Variable para guardar los ataques disponibles
        EnemyAttack[] currentAttacks = GetEnemyAttacks();
        //Numero de ataques que posee este enemigo
        int numAttacks = currentAttacks.Length;
        //Seleccionamos un ataque de todos los posibles disponibles de forma aleatoria
        int selectAttack = Random.Range(0, numAttacks);
        //Indicamos a la variable cual sera el ataque actual de este enemigo
        currentAttack = currentAttacks[selectAttack];
    }

    //Corrutina para controlar el tiempo que demora este ataque en realizarse
    IEnumerator CountCurrentTimeToNextAttack()
    {
        //Variable para guardar cuanto tiempo demorara el ataque seleccionado
        float timeToAttack = currentAttack.attackDuration;
        //Esperamos el tiempo que demora en realizarse el ataque
        yield return new WaitForSeconds(timeToAttack);
        //Si el enemigo es de tipo terrestre
        if (enemyType.Equals("Terrestre"))
        {
            //Si el enemigo no hizo ataque en orquesta...
            if (!this.isAttackedOrquestra)
            {
                Debug.Log("La ventana de bloqueo generada por el ataque del enemigo " + this.gameObject.name + " ha terminado");
            }
        }

        if (enemyType.Equals("Aereo"))
        {  
            //Si el enemigo no hizo ataque en orquesta...
            if (!this.isAttackedOrquestra)
            {
                Debug.Log("La ventana de bloqueo generada por el ataque del enemigo " + this.gameObject.name + " ha terminado");
            }
        }
        //Si no hay ataque en orquesta, ni esta bloqueado, esta disponible para un nuevo ataque orquesta y la cantidad de ataques minimos se cumplio
        if (!isAttackedOrquestra && !Orquestra.sharedInstance.blockOrquestra &&
        Orquestra.sharedInstance.timeEndToNewOrquesta && Orquestra.sharedInstance.currentNAttacks == Orquestra.sharedInstance.N)
        {
            //Indicamos a orquesta que evalue si el random esta dentro del rango indicado o no
            Orquestra.sharedInstance.GetRandom();
        }
        
        //Si el ataque en orquesta se activo y no es parte de un ataque en orquesta
        if (Orquestra.sharedInstance.isActiveOrquesta && !isAttackedOrquestra)
        {
            //Obtendremos cual es el siguiente enemigo en atacar
            Enemy newEnemy = Orquestra.sharedInstance.EncontrarSiguienteAttaque();
            //Si es diferente de nulo.
            if (newEnemy != null)
            {
                //Indicamos a este enemigo que acaba de ser parte del ataque en orquesta
                isAttackedOrquestra = true;
                //Indicamos que el numero de ataques para que vuelva a estar disponible el ataque en orquesta, se reinicia a 0.
                Orquestra.sharedInstance.currentNAttacks = 0;
                //Le indicamos a ese nuevo enemigo que sera parte del ataque en orquesta
                newEnemy.isAttackedOrquestra = true;
                //Comenzamos indicando que el tiempo no termina
                Orquestra.sharedInstance.timeEndToNewOrquesta = false;
                //Indicamos al enemigo que ataque
                newEnemy.Attack();
                //Imprimimos por consola
                Debug.Log("El sistema ha originado un ataque orquesta entre el enemigo " + this.gameObject.name + " y " + newEnemy.gameObject.name);
                //El enemigo ACTUAL esperara que termine el ataque del siguiente enemigo
                yield return new WaitForSeconds(newEnemy.currentAttack.attackDuration);
                //Una vez que termine el ataque del segundo enemigo el ataque en orquesta se desactiva
                Orquestra.sharedInstance.isActiveOrquesta = false;
                //Comenzamos la corrutina de control de tiempo para proximo ataque en orquesta
                StartCoroutine(Orquestra.sharedInstance.CountTimeToOrquestra());
            }
            else
            {
                //Una vez que termina el ataque en orquesta, la bloqueamos hasta que cumpla las condicion de numero de ataques
                Orquestra.sharedInstance.blockOrquestra = true;
            }
        }
        else
        {
            //Finalizamos el ataque orquestado
            Orquestra.sharedInstance.FinishAtaqueOrquestado();
        }
        //Si el enemigo es de tipo terrestre
        if (enemyType.Equals("Terrestre"))
        {
            //Orquesta manda señal que acabo ataque terrestre
            Orquestra.sharedInstance.MandarSeñalAcaboTerrestre();
        }
        //Si el enemigo es de tipo aereo
        if (enemyType.Equals("Aereo"))
        {   
            //Orquesta manda señal que acabo ataque terrestre
            Orquestra.sharedInstance.MandarSeñalAcaboAereo();
        }
    }

}
