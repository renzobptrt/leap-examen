﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{   
    [SerializeField] private int life = 100;
    private int damageMelee = 10;
    private int damageInterlink = 40;
    
    public bool isActiveToInterlink = true;


    public void OnMeleeAttackConnected(){
        DoDamage(damageMelee);
        bool goInterlink = probabilidad(4);
        if(goInterlink){
            OnInterlinkAtttackConnected();    
        }
    }

    public void OnInterlinkAtttackConnected(){
        DoDamage(damageInterlink);
        isActiveToInterlink = false;
        Player.sharedInstance.N--;
        bool goInterlink = probabilidad(10);
        if(goInterlink && Player.sharedInstance.N > 0)
            //Evaluara desde su posición quien es el mas cercano
            Player.sharedInstance.Interlink(this.transform.position);
        else
            Player.sharedInstance.N = 10;
    }

    public void DoDamage(int amount){
        this.life -= amount;
        if(life < 0)
            //Ya no sera acto para Interlink
            isActiveToInterlink = false;
            //Como Destroy puede ser muy pesado para dispositivo, procederemos a mandarlo fuera de la vista de la camara
    }

    bool probabilidad(int prob){
        int num = Random.Range(0,prob);
        if(num == 1)
            return true;
        else
            return false;
    }
}
