﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Animator))]
public class CharacterController : MonoBehaviour
{
    //Features
    //Run
    [SerializeField] private int speedMov = 1;
    private float vertical;
    private float horizontal;

    //Roll
    [SerializeField] private int speedRoll = 1;
    private float countRool = 0 ; 

    //Jump
    [SerializeField] private float jumpForce = 300f;
    private float axisY;
    private bool isGround = true;

    //Attack
    [SerializeField] private int numberOfClicksMT = 0;
    [SerializeField] private int numberOfClicksRT = 0;
    [SerializeField] private int numberOfClicksMA = 0;
    private float lastClickedTime = 0;
    private float maxComboDelay = 0.9f;

    //General moving
    private Vector3 controlMov;
    private bool facingRight;


    //Aninmator parametres
    private bool isRolling = false;
    private bool isFalling = false;
    private bool isAttacking = false;
    private int attackMeleeTierra = 0;
    private int attackRangoTierra = 0;
    private int attackMeleeAire = 0;

    //Components
    private Rigidbody2D rb;
    private SpriteRenderer sp;
    private Animator an;

    private void Awake() {
        rb = this.GetComponent<Rigidbody2D>();
        rb.Sleep();
        sp = this.GetComponent<SpriteRenderer>();
        an = this.GetComponent<Animator>();    
    }


    void Update(){
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
        an.SetFloat("Speed", Mathf.Abs(horizontal!=0 ? horizontal : vertical));
        an.SetBool("IsGround",isGround);
        an.SetBool("IsRolling",isRolling);
        an.SetBool("IsFalling", isFalling);
        an.SetInteger("IsAttackMT", attackMeleeTierra);
        an.SetInteger("IsAttackRT",attackRangoTierra);
        an.SetInteger("IsAttackMA", attackMeleeAire);
        an.SetBool("IsAttack",isAttacking);
    }

    // Update is called once per frame
    void FixedUpdate()
    {   
        //Attack
        if( Time.time - lastClickedTime > maxComboDelay){
            numberOfClicksMT = 0;
            numberOfClicksRT = 0;
            numberOfClicksMA = 0;
            attackMeleeTierra = 0;
            attackRangoTierra = 0;
            attackMeleeAire = 0;
            isAttacking = false;
        }

        if(attackMeleeAire == 0 && !isGround && !isAttacking){
            rb.gravityScale = 1.5f;
        }

        if(numberOfClicksMT == 3)
            numberOfClicksMT = 0;
        if(numberOfClicksRT == 2)
            numberOfClicksRT = 0;
        if(numberOfClicksMA == 3)
            numberOfClicksMA = 0;

        if(Input.GetKeyDown(KeyCode.J) && isGround){
            isAttacking = true;
            numberOfClicksRT = 0;
            attackRangoTierra = 0;
            lastClickedTime = Time.time;
            numberOfClicksMT++;
            attackMeleeTierra = numberOfClicksMT;
            Debug.Log("Ataque Melee Tierra : " + attackMeleeTierra);
        }

        else if(Input.GetKeyDown(KeyCode.I) && isGround){
            isAttacking = true;
            numberOfClicksMT = 0;
            attackMeleeTierra = 0; 
            lastClickedTime = Time.time;
            numberOfClicksRT++;
            attackRangoTierra = numberOfClicksRT;
            Debug.Log("Attaque Rango Tierra : " + attackRangoTierra);
        }

        else if(Input.GetKeyDown(KeyCode.J) && (!isGround || (!isGround && isFalling))){
            isAttacking = true;
            lastClickedTime = Time.time;
            rb.gravityScale = 0;
            rb.velocity = new Vector2(0f,0f);
            numberOfClicksMA++;
            attackMeleeAire = numberOfClicksMA;
        }

        //Saltar
        if(transform.position.y <= axisY)
            OnLanding();

        if(Input.GetKey(KeyCode.Space) && isGround){
            axisY = transform.position.y;
            isGround = false;
            rb.gravityScale = 1.5f;
            rb.WakeUp();
            rb.AddForce(new Vector2(transform.position.x, jumpForce));
            
            if(rb.velocity.y < 0.1)
                isFalling = true;
        }
        //Correr
        if(!isRolling || !isAttacking){
            controlMov = new Vector3(horizontal * speedMov, vertical * speedMov , 0.0f);
            transform.position = transform.position + controlMov * Time.deltaTime;
        }

        Flip(horizontal);
        
        //Roll
        if(Input.GetKey(KeyCode.L) && !isRolling){
            isRolling = true;
            controlMov = new Vector3(horizontal * speedRoll, vertical * speedRoll, 0.0f);
            countRool = 1f;
        }

        if(countRool > 0 ){
            countRool = countRool - Time.deltaTime;
            if(countRool <=0){
                isRolling = false;
                controlMov = new Vector3(horizontal * speedMov, vertical * speedMov, 0.0f);
            }
        }
    }

    //Cambiar direccion
    void Flip(float horizontal){
        if(horizontal < 0 && !facingRight || horizontal > 0 && facingRight){
            facingRight = !facingRight;
            Vector3 scale = transform.localScale;
            scale.x *= -1; 
            transform.localScale = scale;
        } 
    }

    void OnLanding(){
        isGround = true;
        rb.gravityScale = 0f;
        rb.Sleep();
        axisY = transform.position.y;
    }

}
