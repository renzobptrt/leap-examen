﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private bool isAttack = false;
    public List<Enemy> activeEnemies = null;
    public int N = 10; //maximos ataques del interlink
    public float maxDistanceToInterLink = 20;

    public static Player sharedInstance;

    void Awake(){
        if(sharedInstance!=null)
            Destroy(this.gameObject);
        else
            sharedInstance = this;
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.J)){
            AttackMelee();
        }
    }

    void AttackMelee(){
        isAttack = true;
        //Activar Animacion de Melee, el cual tiene la espada (por ejemplo) que hara daño al enemigo 
        //Entra a la clase MeleeDamage 
    }


    public void Interlink(Vector3 position){
        float distanceTarget = 30;
        Enemy targetEnemy = null;
        for(int i=0 ; i < activeEnemies.Count;i++){
            if(activeEnemies[i].isActiveToInterlink){
                //La distancia horizontal, evaluara tanto si el nuevo enemigo esta a la izquierda como derecha
                float distance = Mathf.Abs(position.x - activeEnemies[i].transform.position.x);
                if(distance < maxDistanceToInterLink && distance < distanceTarget){
                    distanceTarget = distance;
                    targetEnemy = activeEnemies[i];
                }
            }
        }
        if(targetEnemy!=null)
            targetEnemy.OnInterlinkAtttackConnected();
    }
}
