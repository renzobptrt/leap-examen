﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveElement : MonoBehaviour
{
    //El tubo a rotar
    private Transform target;
    public float rotateTime = 3.0f;
    public float rotateDegrees = 90.0f;
    private bool rotating = false;
    private Vector3 rotation = new Vector3(0,0,1);

    void Start(){
        target = this.transform;
    }

    //Cuando doy click sobre el objeto
    private void OnMouseDown(){
    
        if (!rotating)
        {   
            //Iniciamos la corutina de rotación
            StartCoroutine(Rotate(this.transform, target, rotation, -rotateDegrees, rotateTime));
        }
    }
    
    //Rotamos el tubo en 90°
    private IEnumerator Rotate(Transform camTransform, Transform targetTransform, Vector3 rotateAxis, float degrees, float totalTime)
    {
        if (rotating)
            yield return null;
        rotating = true;

        //Rotacion
        Quaternion startRotation = camTransform.rotation;
        //Posicion
        Vector3 startPosition = camTransform.position;
        // Get end position;
        //Rotacion en 90° en el eje elegido
        transform.RotateAround(targetTransform.position, rotateAxis, degrees);
        //Final de la rotacion
        Quaternion endRotation = camTransform.rotation;
        Vector3 endPosition = camTransform.position;
        //Setemos valores al objeto que  vamos a rotar
        camTransform.rotation = startRotation;
        camTransform.position = startPosition;

        float rate = degrees / totalTime;

        //Start Rotate
        for (float i = 0.0f; Mathf.Abs(i) < Mathf.Abs(degrees); i += Time.deltaTime * rate)
        {   
            //Rotamos la figura 
            camTransform.RotateAround(targetTransform.position, rotateAxis, Time.deltaTime * rate);
            yield return null;
        }
 
        camTransform.rotation = endRotation;
        camTransform.position = endPosition;
        rotating = false;
    }

}
