﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    public float attackDuration = 0.0f;
    public string[] attackTags;
    public string reactionTags;

    //Constructor para crear un nuevo ataque de enemigo
    public EnemyAttack(float attackDuration, string[] attackTags, string reactionTags){
        this.attackDuration = attackDuration;
        this.attackTags = attackTags;
        this.reactionTags = reactionTags;
    }
}
