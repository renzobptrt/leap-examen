﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orquestra : MonoBehaviour
{
    //Singleton del Orquestador, ya que solo puede existir uno.
    public static Orquestra sharedInstance;
    //Lista de enemigos activos para poder ser parte de la orquesta
    public List<Enemy> activeEnemies;
    
    public int N;   /*número de ataques mínimos necesarios luego de un ataque de orquesta para
                    ejecutar otro ataque de orquestra*/
    public int R;   /*Variable de rango random que determina si un ataque de orquesta puede ser
                    realizado o no.*/
    public int T;   /*Tiempo mínimo en el que un ataque de orquesta no puede ser activado luego 
                    de que el sistema ha activado otro ataque de orquestra*/

    //Numero de ataques actuales realizado por los enemigos, como condición de activación para proximo ataque orquestado
    public int currentNAttacks = 0;
    //Variable booleana que me permitirá indicar si el tiempo de espera para un nuevo ataque orquestado terminó o no.
    public bool timeEndToNewOrquesta = true; 
    //Variable para guardar los tags del ataque del ultimo enemigo en haber realizado dicha acción
    public string[] lastTagOfEnemy;
    //Variable para indicar si el ataque orquestado esta activado actualmente o no.
    public bool isActiveOrquesta = false;
    //Variable que nos servirá para poder desbloquear la realización de un proximo ataque orquestado, una vez cumplidas las condiciones.
    public bool blockOrquestra = false;


    //Numero máximo del rango, del cual saldrá el numero a evaluar dentro del rango pedido.
    [SerializeField] private int maxRange = 0;

    private void Awake() {
        //Evaluamos si existen dos o objetos o más de tipo Orquestra en la escena
        if(sharedInstance!=null)
            Destroy(this.gameObject);
        else
            sharedInstance = this;    
    }

    //Funcion que recibe como parametro el enemigo que realizo el ultimo ataque
    public void RecibirSeñal(Enemy en){
        //Se guardarán los tag
        lastTagOfEnemy = en.currentAttack.attackTags;
        //Mandamos la señal correspondiente a todos los enemigos
        if(en.enemyType.Equals("Terrestre")){
            MandarSeñalTerrestre();
        }
        else if(en.enemyType.Equals("Aereo")){
            MandarSeñalAereo();
        }

    }

    //Función que permite activar la orquesta 
    public void GetRandom(){
        //Recibimos el numero aleatorio
        int random = Random.Range(0, maxRange);
        //Si el numero pertenece al rango indicado
        if( random<= this.R){
            //Activamos el ataque en orquesta
            this.isActiveOrquesta = true;
        }
    }

    //Funcon que permite indicar a los enemigos que realizaron el ataque en orquesta que este finalizo
    public void FinishAtaqueOrquestado(){
        foreach(Enemy en in activeEnemies){
            en.isAttackedOrquestra = false;
        }
    }

    //Funcion para buscar el proximo enemigo disponible para realizar el atque en orquesta
    public Enemy EncontrarSiguienteAttaque(){
        //Evaluamos en la lista de enemigos disponibles
        foreach(Enemy en in activeEnemies){
            //Evaluamos en los tags indicados del ultimo enemigo que realizo el ataque
            for(int i = 0; i< lastTagOfEnemy.Length;i++){
                /*Si el tag de reacción del enemigo coincide con uno de los tag de ataque del ultimo
                enemigo y si este enemigo no ha realizado el ataque en orquesta previamente.
                */
                if(en.currentAttack.reactionTags.Equals(lastTagOfEnemy[i]) && !en.isAttackedOrquestra){
                    //Si cumple las condiciones, retornamos el enemigo
                    return en;
                }
            }
        }
        //Si no encuentra algun enemigo disponible, retornara nulo
        return null;
    }
    
    //Señales
    //Indica a todos los enemigos que un enemigo de tipo aereo realizo un ataque
    void MandarSeñalAereo(){
        foreach(Enemy en in activeEnemies){
            //Cada enemigo sabra que otro enemigo de ataque aereo realizo un ataque
            en.isOtherAereoAttack = true;
        }
    }

    //Indica a todos los enemigos que un enemigo de tipo terrestre realizo un ataque
    void MandarSeñalTerrestre(){
        foreach(Enemy en in activeEnemies){
            //Cada enemigo sabra que otro enemigo de ataque terrestre realizo un ataque
            en.isOtherTerrestreAttack = true;
        }
    }

    //Indica a todos los enemigos que un enemigo de tipo aereo acabo un ataque
    public void MandarSeñalAcaboAereo(){
        foreach(Enemy en in activeEnemies){
            //Cada enemigo sabra que otro enemigo de ataque aereo acabo un ataque
            en.isOtherAereoAttack = false;
        }
    }
    
    //Indica a todos los enemigos que un enemigo de tipo terrestre acabo un ataque
    public void MandarSeñalAcaboTerrestre(){
        foreach(Enemy en in activeEnemies){
            //Cada enemigo sabra que otro enemigo de ataque terrestre acabo un ataque
            en.isOtherTerrestreAttack = false;
        }
    }

    //Corutina que nos servirara para poder tener el control del tiempo que esperara un ataque en orquesta para estar disponible nuevamente
    public IEnumerator CountTimeToOrquestra(){
        //Comenzamos indicando que el tiempo no termina
        this.timeEndToNewOrquesta = false;
        //Espera el tiempo indicado por el diseñador
        yield return new WaitForSeconds(T);
        //Indicamos que el tiempo de espera a concluido.
        this.timeEndToNewOrquesta = true;
        Debug.Log("La ventana de bloqueo de ataque en orquesta ha terminado");
    }
}
