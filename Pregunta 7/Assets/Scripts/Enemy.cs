﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    //Posicion del enemigo
    public Vector3 position;
    //Variable para indicar si esta siendo renderizado o no por la camara    
    public bool isRendering;
    //Camara principal  del juego
    public Camera camera;
    //Identificador
    public int id;

    //Seteamos el valor de su posicion
    private void Awake()
    {
        position = this.transform.position;
    }

    void Start()
    {
        //Evaluamos la posicion respecto a la camara
        Vector3 viewPos = camera.WorldToViewportPoint(position);
        if (viewPos.x < 0 || viewPos.x > 1 || viewPos.y < 0 || viewPos.y > 1)
        {   
            //Si esta siendo rendecerizado
            isRendering = false;
        }
        else
        {   
            //No esta siendo renderizado
            isRendering = true;
            //Lo añadimos a la lista del enemigos activos del player
            Player.sharedInstance.activeEnemies.Add(this);
        }
    }
}
