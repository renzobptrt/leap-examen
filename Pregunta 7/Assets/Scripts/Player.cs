﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/* 
 * Explicacion textual: 
 * El player comienza mirando para una direccion que en este caso es el forward, por tanto el player
 * de una manera logica, solo puede ver dentro de un campo de vision el cual, en un principio el no
 * sabe si estos son activos en la escena o no. Si el player logra detectar enemigos, pasara a 
 * comparar con su lista de Enemigos, si estos pertenecen a su dicha lista para que sepa que si 
 * estan disponibles para poder concentrarse en uno de ellos. Si no detecta ninguno en un principio,
 * se voltera y evaluara a su "espalda", luego de ello si detecta enemigos buscara el mas cercano
 * sino esto indicara de que no hubo ningun enemigo disponible dentro de su campo de vision
 */
public class Player : MonoBehaviour
{   
    //Patron singleton
    public static Player sharedInstance;

    //Features
    //Posicion del player
    public Vector3 pos;
    //Direccion que esta mirando el player
    public Vector3 forward;
    //Distancia del campo de vision
    private float distance = 20.0f;
    //Grado que tendra el campo de vision
    public float gradesOfFieldOfView = 0.0f;

    //Targets
    public List<Enemy> activeEnemies;
    //Enemigo el cual del player se va a concentrar
    public Enemy enemyLook;

    void Awake()
    {
        if (sharedInstance != null)
            Destroy(this.gameObject);
        else
            sharedInstance = this;
    }


    void Start()
    {   
        //Si hay enemigos activos en la escena...
        if (activeEnemies != null)
        {   
            //Comenzara a buscar enemigos
            enemyLook = LookOnSystem(pos, forward, activeEnemies);
            if (enemyLook != null)
            {   
                //Si encuentra, imprimira
                Debug.Log("Se elegio el enemigo: " + enemyLook.id);
            }
        }
    }
    
    //Funcion que devolverá todos los enemigos que esten dentro del campo de visión del player
    List<Enemy> FieldOfView(Vector3 posPlayer, Vector3 directionPlayer, float grades, float distance)
    {
        List<Enemy> enemiesDetected = null;
        //Crea el campo de visión según los grados mandados por parametros
        //Si detecta enemigos, los devuelve como una lista de enemigos detectados
        return enemiesDetected;
    }

    //Función que devolverá el enemigo el cual el Player se concentrara
    Enemy LookOnSystem(Vector3 posPlayer, Vector3 directionPlayer, List<Enemy> activesEnemies)
    {   
        //Lista que contendra la lista de enemigos detectados
        List<Enemy> getEnemies = FieldOfView(posPlayer, directionPlayer, gradesOfFieldOfView, distance);
        //Lista que contendra la lista de enemigos detectados pero que si estan siendo rendeerizados
        List<Enemy> getEnemiesActives = null;
        //Distancia usada para poder asignar al enemigo mas cerca del player
        float minDistance = 1000.0f;
        //Inicializamos un objeto tipo enemigo para poder asignarle el valor correspondiente
        Enemy lookEnemy = null;
        //Si detecta enemigos en la direccion que el player inicia...
        if (getEnemies != null)
        {   
            //A cada enemigo que a sido detectado
            foreach (Enemy en in getEnemies)
            {   
                foreach (Enemy enemyAct in activesEnemies)
                {   
                    //Lo evaluara si pertenece a la lista de enemigos activos en la escena que se identifican por su id
                    if (en.id == enemyAct.id)
                        //Lo añadimos a la  la lista de enemigos detectados que si estan siendo rendeerizados
                        getEnemiesActives.Add(en);
                }
            }
        }
        //Si no detecta enemigos en la direccion que el player inicia...
        else
        {   
            //Player cambia de direccion
            directionPlayer = new Vector3(directionPlayer.x * -1, directionPlayer.y * -1, directionPlayer.z);
            //Evalua denuevo si encuentra enemigos en su campo de vision
            getEnemies = FieldOfView(posPlayer, directionPlayer, gradesOfFieldOfView, distance);
            //Si detecta enemigos...
            if (getEnemies != null)
            {   
                 //A cada enemigo que a sido detectado
                foreach (Enemy en in getEnemies)
                {
                    foreach (Enemy enemyAct in activesEnemies)
                    {
                        //Lo evaluara si pertenece a la lista de enemigos activos en la escena que se identifican por su id
                        if (en.id == enemyAct.id)
                            //Lo añadimos a la  la lista de enemigos detectados que si estan siendo rendeerizados
                            getEnemiesActives.Add(en);
                    }
                }
            }
            //Si en este segundo caso no detecto ningun enemigo, retornamos nulo
            else
            {
                return null;
            }
        }

        //Luego de detectar los enemigos correctos, debemos elegir el que esta mas cerca.
        foreach (Enemy enemyAct in getEnemiesActives)
        {   
            //Evaluamos las distancias de cada uno con respecto a la posicion del player
            float rightDistance = Vector3.Distance(posPlayer, enemyAct.transform.position);
            //Si es menor que la distancia mininima actual
            if (rightDistance <= minDistance)
            {   
                //Seteamos la nueva distancia
                distance = rightDistance;
                //Seteamos el enemigo que por el momento se encuentra mas cerca del player
                lookEnemy = enemyAct;
            }
        }
        //Retornamos el enemigo que se encuentra mas cerca del player
        return lookEnemy;
    }
}
